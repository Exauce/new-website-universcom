/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application





// Jquery, Popper, Bootstrap
import './styles/js/vendor/modernizr-3.5.0.min';
import 'jquery';
import './styles/js/popper.min';
import 'bootstrap'

const $ = require('jquery');
const jQuery = require('jquery');


// Jquery Mobile Menu

import './styles/js/jquery.slicknav.min';

//  Jquery Slick , Owl-Carousel Plugins
import './styles/js/owl.carousel.min';
import './styles/js/slick.min';

// One Page, Animated-HeadLin
import './styles/js/wow.min';
import './styles/js/animated.headline';
import './styles/js/jquery.magnific-popup';


// Date Picker
import './styles/js/gijgo.min';

// Nice-select, sticky
import './styles/js/jquery.nice-select.min';
import './styles/js/jquery.sticky';

// Progress

import './styles/js/jquery.barfiller';

// counter , waypoint,Hover Direction

import './styles/js/jquery.counterup.min';
import './styles/js/waypoints.min';
import './styles/js/jquery.countdown.min';
import './styles/js/hover-direction-snake.min';


//  contact js

import './styles/js/contact';
import './styles/js/jquery.form';
import './styles/js/jquery.validate.min';
import './styles/js/mail-script';
import './styles/js/jquery.ajaxchimp.min';

// Jquery Plugins, main Jquery
import './styles/js/plugins';
import './styles/js/main.js';

import './bootstrap';

