<?php

namespace App\Repository;

use App\Entity\Offre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Offre>
 *
 * @method Offre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offre[]    findAll()
 * @method Offre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OffreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offre::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Offre $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Offre $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Offre[] Returns an array of Offre objects
    //  */

    public function findOffreEnabled()
    {
        return $this->createQueryBuilder('o')
            ->join("o.typeOffre","type_offre")
            ->andWhere('o.enable= :val')
            ->setParameter('val', true)
            ->orderBy('o.id', 'DESC')
            ->select("o.slug", "o.initiale","o.createAt","o.title","o.description","type_offre.name")
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOffreBySlug($slug)
    {
        return $this->createQueryBuilder('o')
            ->join("o.typeOffre","type_offre")
            ->andWhere('o.slug= :val')
            ->setParameter('val', $slug)
            ->orderBy('o.id', 'DESC')
            ->select("o.slug","o.initiale","o.createAt","o.title","o.description","type_offre.name")
            ->getQuery()
            ->getSingleResult()
            ;
    }


    /*
    public function findOneBySomeField($value): ?Offre
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
