<?php

namespace App\Repository;

use App\Entity\Blog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Blog>
 *
 * @method Blog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Blog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Blog[]    findAll()
 * @method Blog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Blog::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Blog $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Blog $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Blog[] Returns an array of Blog objects
    //  */

    public function findEnabled()
    {
        return $this->createQueryBuilder('b')
            ->innerJoin("b.typeBlog","t")
            ->andWhere('b.enable = :val')
            ->setParameter('val', true)
            ->orderBy('b.id', 'DESC')
            ->select("b.id","b.slug", "b.author","b.title","b.file","b.content","b.createAt","b.updateAt","t.name","b.description")
            ->getQuery()
            ->getResult()
        ;
    }



    public function findOneBySlug($slug)
    {
        return $this->createQueryBuilder('b')
            ->join("b.typeBlog","type_blog")
            ->andWhere('b.slug = :val')
            ->setParameter('val', $slug)
            ->select(
                "b.id",
                "b.slug",
                "b.description",
                "b.title",
                "b.content",
                "b.file",
                "b.createAt",
                "b.author",
                "type_blog.name")
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

}
