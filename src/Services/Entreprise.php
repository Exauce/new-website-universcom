<?php

namespace App\Services;

use App\Repository\TeamsRepository;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class Entreprise
{
    /**
     * @var string
     */
 private $mailContact;
    /**
     * @var string
     */
 private $phoneNumber;
    /**
     * @var string
     */
 private $adresse;
    /**
     * @var ContainerBagInterface
     */
 private $params;

    /**
     * @var array
     */
 private $avisClient = [];



 public function __construct(ContainerBagInterface $params, private AdminService $adminService, private TeamsRepository $temsRepository)
 {
     $this->mailContact= "contact@universcom.io";
     $this->phoneNumber= "77 249 96 84";
     $this->adresse= "Mbour- Thiès Sénégal";
     $this->params=  $params;

     $this->avisClient = $this->adminService->getAvisClient();


 }

    /**
     * @return string
     */
    public function getMailContact(): string
    {
        return $this->mailContact;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @return string
     */
    public function getAdresse(): string
    {
        return $this->adresse;
    }

    /**
     * @return string
     */
    public function getBaseUrlAdmin(): string
    {
        return $this->params->get('site_url');
    }

    /**
     * @return array
     */
    public function getAvisClient(): array
    {
        return $this->avisClient;
    }

    /**
     * @return string
     */
    public function getBaseUrlSite(): string
    {
        return $this->params->get('front_url');
    }

/**
 * @return array
 */
    public function getTeams() : array {


     return $this->temsRepository->findBy(["enabled"=>true]);
}




}