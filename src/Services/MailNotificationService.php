<?php

namespace App\Services;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
class MailNotificationService
{

    /**
     * @var TransportInterface
     */
    private $mailer;


    CONST TO_MAIL = "contact@universcom.io";

    public function __construct(TransportInterface $mailer)
    {

        $this->mailer = $mailer;


    }

    public  function notify($template,$template_values) : bool{

        try {

            $mail = ( new TemplatedEmail())
                ->subject($template_values["subject"])
                ->from('noreply@universcom.io')
                ->to(self::TO_MAIL)
                ->htmlTemplate($template)
                ->context($template_values);

         $this->mailer->send($mail);


        } catch (TransportExceptionInterface $exception) {


            return false;
        }


        return true;
    }


}