<?php

namespace App\Services;

use App\Repository\BlogRepository;
use App\Repository\ClientRepository;
use App\Repository\CommentaireRepository;
use App\Repository\OffreRepository;
use App\Repository\UserRepository;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AdminService
{

    public function __construct(
        private $siteUrl,
    private ClientRepository $client,
    private BlogRepository $blogs,
    private UserRepository $users,
    private CommentaireRepository $commentaire,
    private OffreRepository  $offres
)
    {


    }

    public function getBlogData(){

       return $this->blogs->findBy(["enable"=>true]);

    }
    public function getSingleDataBlog($slug){

    return $this->blogs->findOneBy(["slug"=>$slug]);


    }

    public function getOffreData(){

    return $this->offres->findBy(["enable"=>true]);
    }

    public function getSingleDataOffre($slug){

     return $this->offres->findOneBy(["slug"=>$slug]);


    }

    public function getAuthor($username){

    return $this->users->findOneBy(["email"=>$username]);



    }

    public function getCommentaireBlog($id_blog){


    return $this->commentaire->findBy(["blog"=>$id_blog]);


    }
    public function getAvisClient() : array {

   return $this->client->findBy(
          [
              "enbledAvis"=>true
          ]
      );


    }

}