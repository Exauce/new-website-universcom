<?php


namespace App\Command;


use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateUserCommand
 * @package App\Command
 */

class CreateUserCommand extends Command
{
    private $entityManager;
    private $userRepository;
    private $passwordEncoder;

    protected static $defaultName = 'bot:addUser';
    private const USERS = array(
        array(
            'email' => 'sarr@universcom.io',
            'roles' => ["ROLE_USER", "ROLE_ADMIN"],
            "nom"=>"Sarr",
            "prenom"=>"Mouhamédoune",
            "fonction"=>"Redacteur web & SEO",
            "avatar"=>"sarr.png",
            "linkedin"=>"https://www.linkedin.com/"
        ),

        array(
            'email' => 'mouhamadou.gueye@universcom.io',
            'roles' => ["ROLE_USER", "ROLE_ADMIN"],
            "nom"=>"Gueye",
            "prenom"=>"Mouhamadou",
            "fonction"=>"Executive VP at UNIVERSCOM",
            "avatar"=>"sarr.png",
            "linkedin"=>"https://www.linkedin.com/"
        ),

    );

    public function __construct(EntityManagerInterface $entityManager,
                                UserRepository $repository,
                                UserPasswordHasherInterface $hasherEncoder
    )
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->userRepository = $repository;
        $this->passwordEncoder = $hasherEncoder;
    }

    protected function configure()
    {

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach (self::USERS as $u) {
            if (!$this->userRepository->findOneBy(["email" => $u["email"]])) {
                $user = (new  User())
                    ->setEmail($u["email"]);
                $user->setPassword($this->passwordEncoder->hashPassword($user,"MicroCode22"))
                    ->setRoles($u["roles"])
                    ->setNom($u["nom"])
                    ->setPrenom($u["prenom"])
                    ->setFonction($u["fonction"])
                    ->setAvatar($u["avatar"])
                    ->setUrlLinkedin($u["linkedin"]);



                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }


        }





        return Command::SUCCESS;
    }
}