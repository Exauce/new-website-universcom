<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures  extends Fixture implements FixtureGroupInterface
{
    private $user = array(
        array(
            'email' => 'sidy.diallo@universcom.io',
            'roles' => ["ROLE_USER", "ROLE_ADMIN"],
            "nom"=>"Diallo",
            "prenom"=>"Sidy Mohamed",
            "fonction"=>"Directeur de projet",
            "avatar"=>"sidy.png",
            "linkedin"=>"https://www.linkedin.com/in/sidy93"
        ),

    );

    private  $passwordEncoder;
    public function __construct(UserPasswordHasherInterface  $hasherEncoder)
    {
        $this->passwordEncoder = $hasherEncoder;
    }

    public function load(ObjectManager $manager)
    {


        foreach ($this->user as $u) {
            $user = (new  User())
                ->setEmail($u["email"]);
            $user->setPassword($this->passwordEncoder->hashPassword($user,"MicroCode22"))
                ->setRoles($u["roles"])
                ->setNom($u["nom"])
                ->setPrenom($u["prenom"])
                ->setFonction($u["fonction"])
                ->setAvatar($u["avatar"])
                ->setUrlLinkedin($u["linkedin"]);



            $manager->persist($user);

        }


        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ["UserFixtures"];
    }
}