<?php

namespace App\Event;
use App\Entity\Blog;
use App\Entity\Client;
use App\Entity\Commentaire;
use App\Entity\Offre;
use App\Entity\TypeBlog;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class CreateAtAndUpdateAtSubscriber implements EventSubscriberInterface
{



    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setCretedAt'],
            BeforeEntityUpdatedEvent::class => ['setUpdatedAt'],
        ];
    }

    public  function setCretedAt(BeforeEntityPersistedEvent $event){

        $entity = $event->getEntityInstance();



        if (!$entity instanceof Blog  && !$entity instanceof TypeBlog   && !$entity instanceof Offre && !$entity instanceof Client) return;


        $entity->setCreateAt(new \DateTimeImmutable);
        $entity->setUpdateAt(new \DateTimeImmutable);




    }
    public  function setUpdatedAt(BeforeEntityUpdatedEvent $event){
        $entity = $event->getEntityInstance();

        if (!$entity instanceof Blog  && !$entity instanceof TypeBlog  && !$entity instanceof Offre) return;

        $entity->setUpdateAt(new \DateTimeImmutable);
    }
}