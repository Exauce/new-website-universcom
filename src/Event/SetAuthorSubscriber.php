<?php

namespace App\Event;

use App\Entity\Blog;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class SetAuthorSubscriber implements EventSubscriberInterface
{

    public function __construct(private Security $security)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setAuthor'],

        ];
    }

    public  function setAuthor(BeforeEntityPersistedEvent $event){
        $user = $this->security->getUser();
        $entity = $event->getEntityInstance();



        if ($entity instanceof Blog ) {

            $entity->setAuthor($user->getUserIdentifier());
        }




    }

}