<?php

namespace App\Controller;

use App\Entity\Candidat;
use App\Entity\Commentaire;
use App\Repository\BlogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentaireController extends AbstractController
{
    public function __construct(private  BlogRepository $blogRepository,private EntityManagerInterface $em)
    {
    }

    #[Route('/post-commentaire', name: 'app_post_commentaire',methods: ["POST"])]
    public  function postCv(Request $request){


        $content_data =$request->request->all();

        if (count($content_data) > 0) {

           $blog =  $this->blogRepository->find((int)$content_data["id"]);
            $commentaire = (new  Commentaire())->setComment($content_data["comment"])
                ->setUsername($content_data["username"])
                ->setCreateAt(new \DateTimeImmutable)
                ->setBlog($blog);

            $this->em->persist($commentaire);

            $this->em->flush();

            $this->addFlash('comment',"Merci d'avoir renseigner le formauliare de prise de contact. On vous reviens sous peu");


            return  $this->redirect( $this->getParameter("site_url")."blog-single/".$content_data["slug"]."#comment-form");




        }
        return new Response("error",401);
    }


}