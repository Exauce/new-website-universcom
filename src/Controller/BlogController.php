<?php

namespace App\Controller;

use App\Services\AdminService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends  AbstractController
{



    public function __construct(private AdminService $adminService)
    {

    }

    #[Route('/blog',name: 'app_blog')]
    public function  blog() : Response {

        $content = $this->adminService->getBlogData();

        return  $this->render('pages/blog.html.twig', [
            "dataBlog"=>$content
        ]);
    }

    #[Route('/blog-single/{slug}',name: 'app_blog_single')]
    public function  blogSingle($slug) : Response {


        $content = $this->adminService->getSingleDataBlog($slug);





        $author = $this->adminService->getAuthor($content->getAuthor());

        $commentaire = $this->adminService->getCommentaireBlog($content->getId());







        return  $this->render('pages/blogSingle.html.twig', [
            "dataBlog"=>$content,
            "author"=>$author,
            "comment"=>$commentaire
        ]);


    }
}