<?php

namespace App\Controller;

use App\Services\AdminService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarrierController extends  AbstractController
{
   public function __construct(private AdminService $adminService)
   {
   }


    #[Route('/carriere',name: 'app_carrer',methods: ["GET"])]
    public function  carrer() : Response {

        $content = $this->adminService->getOffreData();




        return  $this->render('pages/carrer.html.twig',[
            "dataOffres"=>$content
        ]);
    }




    #[Route('/carriere/{slug}',name: 'app_carrer_single')]
    public function carrerSingle($slug) : Response {

        $content = $this->adminService->getSingleDataOffre($slug);



        return  $this->render('pages/carrerSingle.html.twig',
            [
                "dataOffre"=>$content

            ]
        );
    }


}