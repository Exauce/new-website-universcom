<?php

namespace App\Controller;

use App\Entity\NewsLetter;
use App\Services\AdminService;
use App\Services\MailNotificationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HomeController extends AbstractController
{


    public function __construct(private EntityManagerInterface $em)
    {

    }

    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [

        ]);
    }

   #[Route('/lagence',name: 'app_about')]
    public  function  about():Response {

        return  $this->render("pages/about.html.twig");
    }

    #[Route('/nos-services',name: 'app_service')]
    public function  services() : Response {

        return  $this->render('pages/service.html.twig');
    }
    #[Route('/developpement-it',name: 'app_dev')]
    public function  dev() : Response {

        return  $this->render('pages/dev.html.twig');
    }

    #[Route('/marketing-digital',name: 'app_marketing')]
    public function  marketing() : Response {

        return  $this->render('pages/marketing.html.twig');
    }

    #[Route('/tele-services',name: 'app_tele_service')]
    public function  teleService() : Response {

        return  $this->render('pages/teleService.html.twig');
    }


    #[Route('/nous-contacter',name: 'app_contact')]
    public function  contact() : Response {

        return  $this->render('pages/contact.html.twig');
    }

    #[Route('/parlons-en',name: 'app_devis')]
    public function  devis() : Response {

        return  $this->render('pages/devis.html.twig');
    }

    #[Route('/design',name: 'app_design')]
    public function  design() : Response {

        return  $this->render('pages/design.html.twig');
    }



    #[Route('/contact-form',name: 'app_contact_form',methods: "POST")]
    public function  PostContact(Request $request,MailNotificationService $mailNotificationService) : Response {
        if ($request->isMethod("POST")){

            $dataRequest = $request->request->all();
            $template = "mails/contact.html.twig";

            if ($mailNotificationService->notify($template,$dataRequest)){

                $this->addFlash('notice',"Merci d'avoir renseigner le formauliare de prise de contact. On vous reviens sous peu");

                return  $this->redirectToRoute("app_contact");
            }

        }
        return  $this->redirectToRoute("app_contact");
    }


     #[Route('/rendez-vous-form',name: 'app_rendez_vous__form',methods: "POST")]
     public function  actionRendezVous(Request $request,MailNotificationService $mailNotificationService) : Response {
        if ($request->isMethod("POST")){

            $dataRequest = $request->request->all();
            $template = "mails/rendez_vous.html.twig";
            $dataRequest["subject"] = "Prise de rendez-vous";



            if ($mailNotificationService->notify($template,$dataRequest)){

                $this->addFlash('rendezVous',"Votre rendez-bous a bien été pris en compte . On vous reviens sous peu");


            }
        }
        return  $this->redirectToRoute("app_devis");
}

    #[Route('/appeler-form',name: 'app_appeler__form',methods: "POST")]
    public function  actionCall(Request $request,MailNotificationService $mailNotificationService) : Response {
        if ($request->isMethod("POST")){

            $dataRequest = $request->request->all();
            $template = "mails/call.html.twig";
            $dataRequest["subject"] = "Souhaite être appelé";



            if ($mailNotificationService->notify($template,$dataRequest)){

                $this->addFlash('rendezVous',"Votre rendez-bous a bien été pris en compte . On vous reviens sous peu");


            }
        }
        return  $this->redirectToRoute("app_devis");
    }


    #[Route('/newsletter',name: 'app_new_letter',methods: "POST")]
    public function  PostNewsLetter(Request $request,MailNotificationService $mailNotificationService) : Response {


        if ($request->isMethod("POST")){

            $dataRequest = $request->request->all();
            $template = "mails/newsletter.html.twig";

            $dataRequest["subject"] = "NewsLetter sur nos Offres";
             $newsLetter = ( new NewsLetter())->setEmail($dataRequest["email_newsletter"]);
            $this->em->persist($newsLetter);
            $this->em->flush();
            
            if ($mailNotificationService->notify($template,$dataRequest)){
                $this->addFlash('newsletter',"Enregistrement reussi");


               return  $this->redirectToRoute("app_home",["_fragment"=>"newsletter-form-post"]) ;
            }
        }

        return  $this->redirectToRoute("app_contact");
    }

}
