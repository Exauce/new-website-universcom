<?php

namespace App\Controller;

use App\Entity\Offre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /*#[Route('/sitemap.xml', name: 'app_sitemap', defaults: '{"_format"="xml"}')]*/
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(Request $request) : Response
    {
        $hostname = $request->getSchemeAndHttpHost();

        // On initialise un tableau pour lister les URLs
        $urls = [];

        // On ajoute les URLs "statiques"
        $urls[] = ['loc' => $this->generateUrl('app_home')];
        $urls[] = ['loc' => $this->generateUrl('app_about')];
        $urls[] = ['loc' => $this->generateUrl('app_dev')];
        $urls[] = ['loc' => $this->generateUrl('app_marketing')];
        $urls[] = ['loc' => $this->generateUrl('app_tele_service')];
        $urls[] = ['loc' => $this->generateUrl('app_contact')];
        $urls[] = ['loc' => $this->generateUrl('app_devis')];
        $urls[] = ['loc' => $this->generateUrl('app_design')];

        /*
        foreach ($this->getDoctrine()->getRepository(Offre::class)->findAll() as $carrer) {
            $urls[] = [
                'loc' => $this->generateUrl('carrer-single', [
                    'slug' => $carrer->getSlug(),
                ]),
                'lastmod' => $carrer->getUpdatedAt()->format('Y-m-d'),
            ];
        }*/

         /*
        foreach ($this->getDoctrine()->getRepository(Blogs::class)->findAll() as $blog) {
            $images = [
                'loc' => 'chemin des images'.$blog->getFeaturedImage(), // URL d'image
            ];

            $urls[] = [
                'loc' => $this->generateUrl('blog-single', [
                    'slug' => $blog->getSlug(),
                ]),
                'lastmod' => $blog->getUpdatedAt()->format('Y-m-d'),
                'image' => $images
            ];
        }*/
        
        $response = new Response(
            $this->renderView('sitemap/index.html.twig', ['urls' => $urls,
                'hostname' => $hostname]));
        
        $response->headers->set('Content-Type', 'text/xml');
        
        return $response;
    }
}
