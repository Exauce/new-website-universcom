<?php

namespace App\Controller;

use App\Entity\Candidat;
use App\Services\FileUploaderService;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostCVController extends AbstractController
{
    public function __construct(private FileUploaderService $fileUploaderService, private EntityManagerInterface $em)
    {
    }

    #[Route('/carrer-cv', name: 'app_carrer_cv',methods: ["POST"])]
    public  function postCv(Request $request){


        $cvFile = $request->files->get("file");

        if ($cvFile) {
            $content_data =$request->request->all();


            $cvName = $this->fileUploaderService->upload($cvFile);

            $candidat = (new  Candidat())->setNomComplet($content_data["name"])
                ->setEmail($content_data["email_contact"])
                ->setPoste($content_data["poste"])
                ->setTelephone($content_data["telephone"])
                ->setCv($cvName);

             if (!empty($content_data["motivation"]))
                 $candidat->setMotivation($content_data["motivation"]);

            if (!empty($content_data["slug"]))
                $candidat->setReferenceOffre($content_data["slug"]);

            $this->em->persist($candidat);

            $this->em->flush();

            $this->addFlash('noticeCarrier',"votre candidature a bien été enregistré");

            $url = !empty($content_data["slug"]) ? "carrer-single/".$content_data["slug"] : "carrer";


            return  $this->redirect( $this->getParameter("site_url").$url);




        }
        return new Response("error",401);
    }

}