<?php

namespace App\Controller\Admin;

use App\Entity\Candidat;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Form\Type\FileUploadType;

class CandidatCrudController extends AbstractCrudController
{
    public  const CV_BASE_PATH ="/upload/cv";
    public  const CV_UPLOAD_DIR ="public/upload/cv";
    public static function getEntityFqcn(): string
    {
        return Candidat::class;
    }




    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('nomComplet',"Nom Complet"),
            TextField::new('email',"E-mail"),
            TextField::new('telephone',"Telephone"),
            TextField::new('poste',"Poste"),
            SlugField::new("referenceOffre")->setTargetFieldName('poste'),
            TextEditorField::new('motivation',"Motivation"),
            ImageField::new('cv',"CV ")->hideOnIndex()->setFormType(FileUploadType::class)
                ->setBasePath(self::CV_BASE_PATH)->setUploadDir(self::CV_UPLOAD_DIR),

            TextField::new('cv')->setTemplatePath('admin/fields/document_link.html.twig')->onlyOnIndex()



        ];
    }

}
