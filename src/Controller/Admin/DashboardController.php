<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use App\Entity\Candidat;
use App\Entity\Client;
use App\Entity\NewsLetter;
use App\Entity\Offre;
use App\Entity\TypeBlog;
use App\Entity\TypeOffre;
use App\Entity\Teams;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private AdminUrlGenerator $adminUrlGenerator)
    {
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
       // return parent::index();
        $url = $this->adminUrlGenerator->setController(BlogCrudController::class)
            ->generateUrl();
        return $this->redirect($url);



        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Espace administation ')
            ->disableUrlSignatures();
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
//        yield MenuItem::linkToCrud('BLOG', 'fas fa-list', Blog::class);
//        yield MenuItem::linkToCrud('Type Blog', 'fas fa-list', Blog::class);
        yield MenuItem::subMenu('Blog', 'fas fa-blog')->setSubItems(
            [
                MenuItem::linkToCrud("list","fas fa-list",Blog::class)
                    ->setAction(Crud::PAGE_INDEX),
                 MenuItem::linkToCrud("Type","fas fa-list",TypeBlog::class)
                     ->setAction(Crud::PAGE_INDEX)
            ]
        );

        yield MenuItem::subMenu('Offre', 'fas fa-shield-check')->setSubItems(
            [
                MenuItem::linkToCrud("list","fas fa-list",Offre::class)
                    ->setAction(Crud::PAGE_INDEX),
                MenuItem::linkToCrud("Type","fas fa-list",TypeOffre::class)
                    ->setAction(Crud::PAGE_INDEX)
            ]
        );

        yield MenuItem::linkToCrud('News Letter', 'fas fa-list', NewsLetter::class);
        yield MenuItem::linkToCrud('Candidat', 'fas fa-list', Candidat::class);
        yield MenuItem::linkToCrud('Client', 'fas fa-list', Client::class);
        yield MenuItem::linkToCrud('Teams', 'fas fa-list', Teams::class);
        //yield MenuItem::linkToCrud('Équipe', 'fas fa-list', Teams::class);
    }
}
