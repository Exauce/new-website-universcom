<?php

namespace App\Controller\Admin;

use App\Entity\Client;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ClientCrudController extends AbstractCrudController
{
    public  const BLOG_BASE_PATH ="upload/images/client";
    public  const BLOG_UPLOAD_DIR ="public/upload/images/client";
    public static function getEntityFqcn(): string
    {
        return Client::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
             IdField::new('id')->hideOnForm(),
             TextField::new('raisonSocial',"Raison Social"),
             TextField::new("site","Site internet"),
             TextField::new("nomRepresentant","Nom correspondant"),
             ImageField::new('logo',"Logo ")->setBasePath(self::BLOG_BASE_PATH)->setUploadDir(self::BLOG_UPLOAD_DIR),
             TextField::new("fonctionRepresentant","Fonction Correspondant"),
             BooleanField::new('enbledAvis', "voir l'avis sur le site"),
             TextEditorField::new('avis',"Ce que le client pense de nous"),
        ];
    }

}
