<?php

namespace App\Controller\Admin;

use App\Entity\Teams;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class TeamsCrudController extends AbstractCrudController
{
    public  const USER_BASE_PATH ="upload/images/teams";
    public  const USER_UPLOAD_DIR ="public/upload/images/teams";
    public static function getEntityFqcn(): string
    {
        return Teams::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('nomComplet',"Nom Complet"),
            TextField::new("telephone","Telephone"),
            TextField::new("poste","Poste"),
            ImageField::new('images',"Photo ")->setBasePath(self::USER_BASE_PATH)->setUploadDir(self::USER_UPLOAD_DIR),
            UrlField::new("linkedin","lien Linkedin"),
            UrlField::new("instagram","lien Instagram"),
            UrlField::new("twitter","lien Twitter"),
            BooleanField::new('enabled', "voir sur le site"),

        ];
    }

}
