<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class BlogCrudController extends AbstractCrudController
{
    public  const BLOG_BASE_PATH ="upload/images/blog";
    public  const BLOG_UPLOAD_DIR ="public/upload/images/blog";

    public static function getEntityFqcn(): string
    {
        return Blog::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('title',"titre"),
            SlugField::new("slug")->setTargetFieldName('title'),
            TextField::new('description',"Description(500 caractère maximum)")->setMaxLength(200),
            ImageField::new('file',"Fichier ")->setBasePath(self::BLOG_BASE_PATH)->setUploadDir(self::BLOG_UPLOAD_DIR),
            BooleanField::new('enable'),
            AssociationField::new('typeBlog')->setQueryBuilder(function (QueryBuilder $queryBuilder)
            {

                $queryBuilder->where("entity.enabled =:active")->setParameter('active',true);
            }),
            TextEditorField::new('content'),
        ];
    }

}
