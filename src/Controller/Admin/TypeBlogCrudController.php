<?php

namespace App\Controller\Admin;

use App\Entity\TypeBlog;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TypeBlogCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TypeBlog::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name'),
            BooleanField::new('enabled'),
            DateTimeField::new('updateAt')->hideOnForm(),
            DateTimeField::new('createAt')->hideOnForm(),
        ];
    }

}
