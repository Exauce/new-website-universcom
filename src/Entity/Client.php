<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ClientRepository::class)]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $raisonSocial;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $site;

    #[ORM\Column(type: 'text', nullable: true)]
    private $avis;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $nomRepresentant;

    #[ORM\Column(type: 'string', length: 255)]
    private $fonctionRepresentant;

    #[ORM\Column(type: 'datetime_immutable')]
    private $createAt;

    #[ORM\Column(type: 'datetime_immutable')]
    private $updateAt;

    #[ORM\Column(type: 'boolean')]
    private $enbledAvis;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $logo;

    public function __construct()
    {
        $this->enbledAvis = false;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRaisonSocial(): ?string
    {
        return $this->raisonSocial;
    }

    public function setRaisonSocial(string $raisonSocial): self
    {
        $this->raisonSocial = $raisonSocial;

        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(?string $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getAvis(): ?string
    {
        return $this->avis;
    }

    public function setAvis(?string $avis): self
    {
        $this->avis = $avis;

        return $this;
    }

    public function getNomRepresentant(): ?string
    {
        return $this->nomRepresentant;
    }

    public function setNomRepresentant(?string $nom_representant): self
    {
        $this->nomRepresentant = $nom_representant;

        return $this;
    }

    public function getFonctionRepresentant(): ?string
    {
        return $this->fonctionRepresentant;
    }

    public function setFonctionRepresentant(string $fonctionRepresentant): self
    {
        $this->fonctionRepresentant = $fonctionRepresentant;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeImmutable $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeImmutable $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getEnbledAvis(): ?bool
    {
        return $this->enbledAvis;
    }

    public function setEnbledAvis(bool $enbledAvis): self
    {
        $this->enbledAvis = $enbledAvis;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function isEnbledAvis(): ?bool
    {
        return $this->enbledAvis;
    }
}
